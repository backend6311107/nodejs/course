const express = require("express");
const app = express();
const port = 8000;
const mongoose = require("mongoose");
mongoose.set('strictQuery', true);
app.use(express.static("app"));
app.use(express.json());

//Khai báo router app
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");

//Khai bao model
// const reviewModel = require("../B2_middleware/app/models/reviewModel");
// const courseModel = require("../B2_middleware/app/models/courseModel");

app.use((req, res, next) => {
    console.log(`Current time ${new Date()}`)
    next();
})

app.use((req, res, next) => {
    console.log(`Request method: ${req.method}`);
    next();
})

// app.get("/", (req, res)=>{
//     console.log("API get");
//     res.json({
//         message: "Example middleware"
//     })
// })

//App sử dụng router
app.use("/api", courseRouter);
app.use("/api", reviewRouter);

mongoose.connect("mongodb://127.0.0.1:27017/ex", (error) => {
    if (error) throw error;
    console.log("Connect MongoDB successfully !!!");
})

app.listen(port, () => {
    console.log(`App đang chạy trên port ${port}`);
})