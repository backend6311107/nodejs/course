const getAllCourseMiddleware = (req, res, next)=>{
    console.log("Get all course middleware");
    next();
}

const createCourseMiddleware = (req, res, next)=>{
    console.log("Create new course middleware");
    next();
}

const getCourseDetaildMiddleware = (req, res, next)=>{
    console.log("Get course by ID middleware");
    next();
}

const updateCourseMiddleware = (req, res, next)=>{
    console.log("Update course middleware");
    next();
}

const deleteCourseMiddleware = (req, res, next)=>{
    console.log("Delete course middleware");
    next();
}

module.exports = {
    getAllCourseMiddleware,
    getCourseDetaildMiddleware,
    createCourseMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
}