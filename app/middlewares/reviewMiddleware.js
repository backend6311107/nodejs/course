const getAllReviewMiddleware = (req, res, next) => {
    console.log("Get all review middleware");
    next();
}

const getReviewByIdMiddleware = (req, res, next) => {
    console.log("Get review by ID middleware");
    next();
}

const getAllReviewOfCourseMiddleware = (req, res, next) => {
    console.log("Get all review of course middleware");
    next();
}

const createReviewMiddleware = (req, res, next) => {
    console.log("Create new review of course middleware");
    next();
}

const updateReviewByIdMiddleware = (req, res, next) => {
    console.log("Update review by ID middleware");
    next();
}

const deleteReviewByIdMiddleware = (req, res, next) => {
    console.log("Delete review by ID middleware");
    next();
}

module.exports = {
    getAllReviewMiddleware,
    getReviewByIdMiddleware,
    getAllReviewOfCourseMiddleware,
    createReviewMiddleware,
    updateReviewByIdMiddleware,
    deleteReviewByIdMiddleware
}