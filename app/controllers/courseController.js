const mongoose = require("mongoose");
const courseModel = require("../../app/models/courseModel");

const getAllCourse = (request, response) => {
    courseModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all course successfully.",
            data: data,

        })

    })
}

const getCourseById = (request, response) => {
    const courseID = request.params.courseId;

    if (!mongoose.Types.ObjectId.isValid(courseID)) {
        return response.status(400).json({
            status: "Bad request",
            message: "CourseID Không hợp lệ !!!"
        })
    }

    courseModel.findById(courseID, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get detail course by ID successfully.",
            data: data
        })
    })

}

const createCourse = (request, response) => {
    const body = request.body;
    //console.log(body)

    if (!body.title) {
        return response.status(400).json({
            status: "Bad request",
            message: "Title Không hợp lệ !!!"
        })
    }
    if (isNaN(body.noStudent) || body.noStudent < 0) {
        return response.status(400).json({
            status: "Bad request",
            message: "noStudent Không hợp lệ !!!"
        })
    }

    const newCourse = {
        _id: mongoose.Types.ObjectId(),
        title: body.title,
        description: body.description,
        noStudent: body.noStudent
    }
    courseModel.create(newCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error !!!",
                message: error.message
            })
        }
        return response.status(201).json({
            status: "Create new course successfully.",
            data: data
        })
    })
}

const updateCourse = (request, response) => {
    const courseID = request.params.courseId;
    const body = request.body;

    if (!mongoose.Types.ObjectId.isValid(courseID)) {
        return response.status(400).json({
            status: `Bad request`,
            message: `CourseId ${courseID} không hợp lệ !!!`
        })
    }
    if (body.title !== undefined && body.title.trim() === "") {
        return response.status(400).json({
            status: `Bad request`,
            message: `title không hợp lệ !!!`
        })
    }
    if (body.noStudent !== undefined && (isNaN(body.noStudent) || body.noStudent < 0)) {
        return response.status(400).json({
            status: `Bad request`,
            message: `noStudent không hợp lệ !!!`
        })
    }

    const updateCourse = {}
    if (body.title !== undefined) {
        updateCourse.title = body.title;
    }
    if (body.description !== undefined) {
        updateCourse.description = body.description;
    }
    if (body.noStudent !== undefined) {
        updateCourse.noStudent = body.noStudent;
    }
    courseModel.findByIdAndUpdate(courseID, updateCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: `Internal server error`,
                message: error.message
            })
        }
        return response.status(200).json({
            status: `Updater CourseID ${courseID} successfully.`,
            data: data
        })
    })
}

const deleteCourse = (request, response) => {
    const courseID = request.params.courseId;

    if (!mongoose.Types.ObjectId.isValid(courseID)) {
        return response.status(400).json({
            status: `Bad request`,
            message: `CourseId ${courseID} không hợp lệ !!!`
        })
    }

    courseModel.findByIdAndDelete(courseID, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: `Internal server error`,
                message: error.message
            })
        }
        return response.status(200).json({
            status: `Delete courseId ${courseID} successfully.`
        })
    })
}

module.exports = {
    getAllCourse,
    getCourseById,
    createCourse,
    updateCourse,
    deleteCourse
}