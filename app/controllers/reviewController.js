const mongoose = require("mongoose");
const reviewModel = require("../models/reviewModel");
const courseModel = require("../models/courseModel");

const getAllReview = (req, res) => {
    reviewModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: "Get all review Successfully.",
            data: data
        })
    })
}

const getReviewById = (req, res) => {
    const reviewID = req.params.reviewId;

    if (!mongoose.Types.ObjectId.isValid(reviewID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `reviewID: ${reviewID} không hợp lệ !`
        })
    }

    reviewModel.findById(reviewID, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Get reviewID: ${reviewID} successfully.`,
            data: data
        })
    })
}

const getAllReviewOfCourse = (req, res) => {
    const courseID = req.params.courseId;

    if (!mongoose.Types.ObjectId.isValid(courseID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Course ID: ${courseID} không hợp lệ !`
        })
    }

    courseModel.findById(courseID).populate("reviews").exec((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return res.status(200).json({
            status: `Get all review of course ID: ${courseID} successfully.`,
            data: data
        })
    })
}

const createReviewOfCourse = (req, res) => {
    const courseID = req.params.courseId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(courseID)) {
        return res.status(400).json({
            status: "Bad request",
            messsage: `CourseID: ${courseID} không hợp lệ !`
        })
    }
    if (!(Number.isInteger(body.stars) && body.stars > 0 && body.stars <= 5)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Rate không hợp lệ"
        })
    }

    const newReview = {
        _id: mongoose.Types.ObjectId(),
        stars: body.stars,
        note: body.note
    }
    reviewModel.create(newReview, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        courseModel.findByIdAndUpdate(courseID, { $push: { reviews: data._id } }, (err) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(201).json({
                status: "Create new review successfully.",
                data: data
            })
        })
    })
}

const updateReviewById = (req, res) => {
    const reviewID = req.params.reviewId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(reviewID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Review ID: ${reviewID} không hợp lệ !`
        })
    }
    if (body.stars !== undefined && !(Number.isInteger(body.stars) && body.stars > 0 && body.stars <= 5)) {
        return res.status(400).json({
            status: "Bad request",
            message: "stars không hợp lệ !"
        })
    }

    const updateReview = {}
    if (body.stars !== undefined) {
        updateReview.stars = body.stars;
    }
    if (body.note !== undefined) {
        updateReview.note = body.note;
    }
    reviewModel.findByIdAndUpdate(reviewID, updateReview, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
        return res.status(200).json({
            status: `Update review by ID: ${reviewID} successfully.`,
            data: data
        })
    })
}

const deleteReviewById = (req, res) => {
    const reviewID = req.params.reviewId;
    const courseID = req.params.courseId;

    if (!mongoose.Types.ObjectId.isValid(reviewID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Review ID: ${reviewID} không hợp lệ !`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(courseID)) {
        return res.staus(400).json({
            status: "Bad request",
            message: `Course ID: ${courseID} không hợp lệ !`
        })
    }

    reviewModel.findByIdAndDelete(reviewID, (error) => {
        if (error) {
            return res.status(500), json({
                status: "Internal server error",
                messasge: error.message
            })
        }
        courseModel.findByIdAndUpdate(courseID, { $pull: { reviews: reviewID } }, (err) => {
            if (err) {
                return res.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return res.status(204).json({
                status: `Delete review by ID: ${reviewID} successfully.`
            })
        })
    })
}

module.exports = {
    getAllReview,
    getReviewById,
    getAllReviewOfCourse,
    updateReviewById,
    deleteReviewById,
    createReviewOfCourse
}