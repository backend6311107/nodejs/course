const express = require("express");
const router = express.Router();
const reviewMiddleware = require("../middlewares/reviewMiddleware");
const reviewController = require("../controllers/reviewController");
router.get("/reviews", reviewMiddleware.getAllReviewMiddleware, reviewController.getAllReview)

router.get("/reviews/:reviewId", reviewMiddleware.getReviewByIdMiddleware, reviewController.getReviewById);

router.get("/courses/:courseId/reviews", reviewMiddleware.getAllReviewOfCourseMiddleware, reviewController.getAllReviewOfCourse);

router.post("/courses/:courseId/reviews", reviewMiddleware.createReviewMiddleware, reviewController.createReviewOfCourse);

router.put("/reviews/:reviewId", reviewMiddleware.updateReviewByIdMiddleware, reviewController.updateReviewById);

router.delete("/courses/:courseId/reviews/:reviewId", reviewMiddleware.deleteReviewByIdMiddleware, reviewController.deleteReviewById);

module.exports = router;