const express = require("express");
const router = express.Router();
const courseMiddleware = require("../../app/middlewares/courseMiddleware");
const courseController = require("../../app/controllers/courseController");

router.get("/courses", courseMiddleware.getAllCourseMiddleware, courseController.getAllCourse)

router.get("/courses/:courseId", courseMiddleware.getCourseDetaildMiddleware, courseController.getCourseById)

router.post("/courses", courseMiddleware.createCourseMiddleware, courseController.createCourse)

router.put("/courses/:courseId", courseMiddleware.updateCourseMiddleware, courseController.updateCourse)

router.delete("/courses/:courseId", courseMiddleware.deleteCourseMiddleware, courseController.deleteCourse)

module.exports = router;